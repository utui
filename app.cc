// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// app.cc
//

#define _GNU_SOURCE 1	// for strsignal
#include "app.h"
#include "rootwin.h"
#include <signal.h>

//----------------------------------------------------------------------

/// Default constructor.
CApplication::CApplication (void)
{
}

/// Initializes the output device.
void CApplication::OnCreate (argc_t, argv_t)
{
    CRootWindow::Instance().OnCreate();
}

/// Deinitializes the output device.
void CApplication::OnDestroy (void)
{
    CRootWindow::Instance().OnDestroy();
}

int CApplication::Run (argc_t argc, argv_t argv)
{
    int rv = EXIT_FAILURE;
    try {
	OnCreate (argc, argv);
	rv = CRootWindow::Instance().Run();
    } catch (exception& e) {
	CRootWindow::Instance().OnError (e);
    } catch (...) {
	cerr << "Unexpected error occurred.\n";
    }
    OnDestroy();
    return (rv);
}


//----------------------------------------------------------------------

/// Called when a signal is received.
static void OnSignal (int sig)
{
    CRootWindow::Instance().OnDestroy();
    cout.flush();
    #if HAVE_STRSIGNAL
	cerr.format ("Fatal error: %s\n", strsignal(sig));
    #else
	cerr.format ("Fatal error: system signal %d received\n", sig);
    #endif
    #ifndef NDEBUG
	cerr << CBacktrace();
    #endif
    exit (128 + sig);	// 128+sig is what bash sets exit value to on signal crash
}

/// Called by the framework on unrecoverable exception handling errors.
static void Terminate (void)
{
    assert (!"Unrecoverable exception handling error detected.");
    raise (SIGABRT);
    exit (EXIT_FAILURE);
}

/// Called when an exception violates a throw specification.
static void OnUnexpected (void)
{
    cerr << "Unexpected exception fatal internal error occured\n";
    Terminate();
}

/// Installs OnSignal as handler for signals.
extern "C" void InstallCleanupHandlers (void)
{
    static const uint8_t c_Signals[] = {
	SIGINT, SIGQUIT, SIGILL,  SIGTRAP, SIGABRT,
	SIGIOT, SIGBUS,  SIGFPE,  SIGSEGV, SIGTERM,
	SIGIO,  SIGCHLD
    };
    for (uoff_t i = 0; i < VectorSize(c_Signals); ++ i)
	signal (c_Signals[i], OnSignal);
    std::set_terminate (Terminate);
    std::set_unexpected (OnUnexpected);
}

