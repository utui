// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// window.cc
//

#include "rootwin.h"
#include "ctrl/menu.h"

//----------------------------------------------------------------------

const Point2d CWindow::pos_CaretOff (-1, -1);

//----------------------------------------------------------------------
// Housekeeping

/// Default constructor.
CWindow::CWindow (void)
: CDocumentClient (),
  m_GC (),
  m_Children (),
  m_WindowRect (),
  m_Focus (NO_FOCUS),
  m_CaretPos (pos_CaretOff)
{
}

/// Virtual destructor.
CWindow::~CWindow (void)
{
    DeleteAllChildren();
}

/// Initializes the window.
void CWindow::OnCreate (void)
{
    foreach (winvec_t::iterator, i, m_Children)
	(*i)->OnCreate();
    SetFlag (f_Created);
}

/// Uninitializes the window.
void CWindow::OnDestroy (void)
{
    ClearFlag (f_Created);
    eachfor (winvec_t::reverse_iterator, i, m_Children)
	(*i)->OnDestroy();
}

//----------------------------------------------------------------------
// Child window maintenance

/// Deletes all child windows.
void CWindow::DeleteAllChildren (void)
{
    SetFocus (NO_FOCUS);
    DeleteAllLayoutItems();
    for (int i = m_Children.size() - 1; i >= 0; --i) {
	OnChildClose (i);
	Delete (m_Children[i]);
    }
    m_Children.clear();
}

/// Adds \p pw to the (owning) child window list.
void CWindow::AddChild (pwin_t pw)
{
    m_Children.push_back (pw);
    if (Flag (f_Created))
	pw->OnCreate();
    if (Flag (f_Resized))
	OnResize (WindowRect());
}

/// Removes \p pw from internal list of children without deleting it.
void CWindow::RemoveChild (pwin_t pw)
{
    foreach (winvec_t::iterator, i, m_Children) {
	if (*i == pw) {
	    OnChildClose (distance (m_Children.begin(), i));
	    --(i = m_Children.erase (i));
	}
    }
}

/// Uses the given descriptors to stuff the container.
pcwidesc_t CWindow::InitFromDesc (pcwidesc_t pwd, pwidget_t parent)
{
    assert (pwd->pfnFactory && "Unexpected end of dialog descriptor list");
    pwidget_t pw = (*pwd->pfnFactory)(pwd->initValue);
    assert (pw && "Widget factories should throw on failure");
    assert ((pwd->nChildren || dynamic_cast<pwin_t>(pw)) && "Layout helper objects must have at least one window to layout");
    if (parent)
	parent->AddLayoutItem (pw);	// non-owning reference pointer for layout
    const size_t ncli ((pwd++)->nChildren);
    if (!ncli)
	AddChild (static_cast<pwin_t>(pw));
    else {
	AddLayoutItem (pw);			// owning pointer in the window object
	for (uoff_t i = 0; i < ncli; ++i)
	    pwd = InitFromDesc (pwd, pw);
    }
    return (pwd);
}

/// Called whenever a child window closes itself.
void CWindow::OnChildClose (uoff_t i)
{
    if (Focus() == i)
	SetFocus (GetNextFocus());
    if (CW(i).Flag (f_Created))
	CW(i).OnDestroy();
}

/// Returns in \p r the area of window \p pw in this window's coordinates.
/// false is returned if \p pw is not in this window's subtree.
bool CWindow::SeenCWRect (Rect& r, pcwin_t pw) const
{
    if (pw == this) {
	r = WindowArea();
	return (true);
    }
    foreach (winvec_t::const_iterator, i, m_Children) {
	if ((*i)->SeenCWRect (r, pw)) {
	    r += (*i)->WindowRect()[0];
	    return (true);
	}
    }
    return (false);
}

/// Searches through ths subtree under this window to find the parent of \p w.
CWindow::pwin_t CWindow::ParentWindowOf (pcwin_t w)
{
    foreach (winvec_t::const_iterator, i, m_Children) {
	if (*i == w)
	    return (this);
	pwin_t pw;
	if ((pw = (*i)->ParentWindowOf (w)))
	    return (pw);
    }
    return (NULL);
}

/// Returns the parent window of this window. Slow.
CWindow::pwin_t CWindow::ParentWindow (void) const
{
    return (CRootWindow::Instance().ParentWindowOf (this));
}

//----------------------------------------------------------------------
// Event handlers

/// Finds the next focus if the current one gives it up.
uint32_t CWindow::GetNextFocus (int dir) const
{
    const size_t ni (m_Children.size());
    if (!ni)
	return (NO_FOCUS);
    if (dir < 0)
	dir += ni;
    uint32_t of = Focus();
    if (of >= ni)
	of = ni - dir;
    uint32_t nf = of;
    // Cycle through all the children looking for something on the tab order.
    while ((nf = (nf+dir)%ni) != of && CW(nf).Flag(f_NoTabOrder));
    // If there was no focus, check if nf is tabbable
    if (nf == of && Focus() == NO_FOCUS && CW(nf).Flag(f_NoTabOrder))
	nf = NO_FOCUS;
    return (nf);
}

/// Called on every event loop iteration.
void CWindow::OnIdle (void)
{
    if (Focus() < m_Children.size() && CW(Focus()).Flag (f_OffersFocus))
	SetFocus (GetNextFocus());
    foreach (winvec_t::iterator, i, m_Children) {
	(*i)->OnIdle();
	if ((*i)->Flag (f_Closed)) {
	    OnChildClose (distance (m_Children.begin(), i));
	    Delete (*i);
	    --(i = m_Children.erase (i));
	}
    }
}

/// Returns the recommended area of the window when \p r is the total available area.
void CWindow::SizeHints (rrect_t wr) const
{
    if (!NLI())
	return;
    Rect r (wr);
    wr -= r[0];
    LI(0).SizeHints (wr);
    wr += r[0];
    simd::pmin (r[1], wr[1]);
}

/// Sets the window rect to \p wr.
void CWindow::OnResize (rcrect_t wr)
{
    m_WindowRect = wr;
    Rect wa (wr);
    wa -= wr[0];
    m_GC.Resize (wa[1][0], wa[1][1]);
    if (NLI())
	LI(0).OnResize (wa);
    SetFlag (f_Resized);
}

/// Routes \p key down the focus path.
void CWindow::OnKey (wchar_t key)
{
    foreach (cmdkvec_t::const_iterator, i, m_CommandKeys)
	if (i->key == key)
	    CMenuBar::Instance()->ExecuteCommand (i->cmd);
    const wchar_t normkey = tolower (key & kvm_KeyMask);
    for (uoff_t i = 0; i < m_Children.size(); ++ i) {
	wchar_t ack = CW(i).FocusAccelKey();
	if ((ack == normkey) | (ack == key)) {
	    SetFocus (i);
	    if (CW(i).Flag (f_NoTabOrder)) {
		SetFocus (GetNextFocus());
		return;
	    }
	    key = kv_Enter;
	}
    }
    if (m_Focus < m_Children.size())
	CW(m_Focus).OnKey (key);
}

/// Handles command \p cmd by forwarding it to child windows.
void CWindow::OnCommand (cmd_t cmd)
{
    foreach (winvec_t::iterator, i, m_Children)
	(*i)->OnCommand (cmd);
}

/// Updates the state of command \p cmd.
void CWindow::OnUpdateCommandUI (rcmd_t rcmd) const
{
    foreach (winvec_t::const_iterator, i, m_Children)
	(*i)->OnUpdateCommandUI (rcmd);
}

/// Draws the window and its children onto the internal gc.
void CWindow::Paint (void)
{
    CaretOff();
    OnDraw (GC());
    foreach (winvec_t::iterator, i, m_Children) {
	(*i)->Paint();
	rcrect_t r ((*i)->WindowRect());
	GC().Image (r[0][0], r[0][1], r.Width(), r.Height(), (*i)->GC().Canvas());
	if (distance (m_Children.begin(), i) == m_Focus && (*i)->CaretPos() != pos_CaretOff)
	    CaretTo ((*i)->WindowRect()[0] + (*i)->CaretPos());
    }
}

//----------------------------------------------------------------------
// Focus management

/// Sets \p f as the focused child window.
void CWindow::SetFocus (uoff_t f)
{
    if (m_Focus == f)
	return;
    if (m_Focus < m_Children.size())
	CW(m_Focus).OnLoseFocus();
    m_Focus = f;
    if (m_Focus < m_Children.size())
	CW(m_Focus).OnGainFocus();
}

/// Called when this window loses focus.
void CWindow::OnGainFocus (void)
{
    SetFlag (f_HasFocus);
    if (m_Focus >= m_Children.size())
	SetFocus (GetNextFocus());
    if (m_Focus < m_Children.size())
	CW(m_Focus).OnGainFocus();
}

/// Called when this window gains focus.
void CWindow::OnLoseFocus (void)
{
    if (m_Focus < m_Children.size())
	CW(m_Focus).OnLoseFocus();
    CaretOff();
    ClearFlag (f_HasFocus);
    ClearFlag (f_OffersFocus);
}

/// Moves the caret to \p pt.
void CWindow::CaretTo (rcpos_t pt)
{
    if (dim_t(pt[0]) >= WindowRect().Width() || dim_t(pt[1]) >= WindowRect().Height())
	m_CaretPos = pos_CaretOff;
    else
	m_CaretPos = pt;
}

