// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// msgbox.h
//

#ifndef MSGBOX_H_43442878202627633668B1BA269E4ED1
#define MSGBOX_H_43442878202627633668B1BA269E4ED1

#include "dialog.h"

/// Types of standard message boxes.
enum EMBType {
    MB_Ok,
    MB_OkCancel,
    MB_YesNo,
    MB_YesNoCancel,
    MB_Retry,
    MB_Last
};

/// Possible return values from CMessageBox::Run
enum EMBReturnValue {
    mbrv_Ok,
    mbrv_Yes	= mbrv_Ok,
    mbrv_Retry	= mbrv_Ok,
    mbrv_Cancel,
    mbrv_Abort	= mbrv_Cancel,
    mbrv_No,
    mbrv_Ignore,
    mbrv_Last
};

/// \class CMessageBox msgbox.h msgbox.h
/// Displays a modal message box.
class CMessageBox : public CDialog {
public:
			CMessageBox (const string& s = string::empty_string, EMBType t = MB_Ok);
protected:
    virtual void	OnKey (wchar_t key);
    virtual void	OnCommand (cmd_t cmd);
private:
    string		m_Text;		///< Message in the box.
    EMBType		m_Type;		///< Button arrangement.
};

//----------------------------------------------------------------------

int MessageBox (const string& s, EMBType t = MB_Ok);

//----------------------------------------------------------------------

#endif

