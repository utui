// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// dialog.h
//

#ifndef DIALOG_H_72C5A9947589648B43ECB6345C1D548B
#define DIALOG_H_72C5A9947589648B43ECB6345C1D548B

#include "ctrl/label.h"
#include "ctrl/button.h"
#include "ctrl/editbox.h"
#include "ctrl/listbox.h"

/// \class CDialog dialog.h dialog.h
///
/// Base class for dialog-like windows, which are usually dynamically-stuffed
/// containers of controls with a visible border, used modally.
///
class CDialog : public CWindow {
public:
			CDialog (void);
    virtual void	OnKey (wchar_t key);
protected:
    virtual void	OnDraw (CGC& gc);
    virtual void	OnChildClose (uoff_t i);
    virtual void	SizeHints (rrect_t wr) const;
};

#endif
