// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// doccli.h
//

#ifndef DOCCLI_H_7F10D8511EA748625506A5FD1391E710
#define DOCCLI_H_7F10D8511EA748625506A5FD1391E710

#include "cmdtarg.h"

class CDocument;

/// \class CDocumentClient doccli.h utui.h
///
/// This defines the interface for a view for accessing a bound document and
/// the interface for a document for binding such a view and sending updates.
///
class CDocumentClient : public CCmdTarget {
public:
    enum {
	f_Updated = CCmdTarget::f_Last,	///< OnInitialUpdate has been called.
	f_ReadOnly,	///< Document can not be changed.
	f_Changed,	///< Made changes to the document.
	f_Last
    };
    typedef CDocument*		pdoc_t;
    typedef const CDocument*	pcdoc_t;
public:
			CDocumentClient (void);
    void		Update (pdoc_t pDoc);
protected:
    inline pdoc_t	Document (void)		{ return (m_pDocument); }
    inline pcdoc_t	Document (void) const	{ return (m_pDocument); }
    template <typename T>
    inline T*		TDocument (void)	{ return (static_cast<T*>(Document())); }
    template <typename T>
    inline const T*	TDocument (void) const	{ return (static_cast<const T*>(Document())); }
    inline virtual void	OnUpdate (void)		{ }
    inline virtual void	OnInitialUpdate (void)	{ OnUpdate(); SetFlag (f_Updated); }
private:
    CDocument*		m_pDocument;	///< Pointer to the document.
};

#endif

