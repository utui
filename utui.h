// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// utui.h
//

#ifndef UTUI_H_21AA3EE670CD0F1F358FD2A763CBB100
#define UTUI_H_21AA3EE670CD0F1F358FD2A763CBB100

#include "utui/app.h"
#include "utui/rootwin.h"
#include "utui/msgbox.h"
#include "utui/doc.h"
#include "utui/ctrl/menu.h"

#endif

