// Copyright (c) 2006 by Mike Sharov <msharov@users.sourceforge.net>
//
// layout.cc
//

#include "layout.h"

//----------------------------------------------------------------------

/// Default constructor.
CLayout::CLayout (void)
{
}

/// Default destructor.
CLayout::~CLayout (void)
{
}

/// Resizes to fit everything into \p r.
void CLayout::OnResize (rcrect_t)
{
}

/// Returns recommended size to fit everything into \p r.
void CLayout::SizeHints (rrect_t wr) const
{
    wr = Rect();
}

/// \brief Deletes all pointers in the layout item list.
/// This is normally called by #CWindow. Non-window layout objects do not own
/// the layout pointers stored in their lists.
///
void CLayout::DeleteAllLayoutItems (void)
{
    foreach (itemvec_t::iterator, i, m_Items)
	Delete (*i);
    m_Items.clear();
}

//----------------------------------------------------------------------

/// Constructs a tile layout with axis \p dir, \p spacing, and margins.
CTileLayout::CTileLayout (ELayoutDir dir, uint8_t spacing, uint8_t lmargin, uint8_t rmargin)
: CLayout (),
  m_Dir (dir),
  m_Spacing (spacing),
  m_LMargin (lmargin),
  m_RMargin (rmargin)
{
}

/// Resizes all items to their tiled locations.
void CTileLayout::OnResize (rcrect_t r)
{
    Rect tr (r), cwr;
    tr[0][m_Dir] += m_LMargin;
    for (uoff_t i = 0; i < NLI(); ++i) {
	LI(i).SizeHints (cwr=tr);
	Size2d cs (cwr.Size());
	tr[1][m_Dir] = tr[0][m_Dir] + cs[m_Dir];
	LI(i).OnResize (tr);
	tr[0][m_Dir] += cs[m_Dir] + m_Spacing;
	tr[1] = r[1];
    }
}

/// Returns the size of the tiled items.
void CTileLayout::SizeHints (rrect_t tr) const
{
    Rect cwr;
    Size2d sz;
    Point2d tl (tr[0]);
    tr[0][m_Dir] += m_LMargin;
    sz[m_Dir] += m_LMargin;
    for (uoff_t i = 0; i < NLI(); ++i) {
	LI(i).SizeHints (cwr=tr);
	Size2d cs (cwr.Size());
	cs[m_Dir] += m_Spacing;
	sz[!m_Dir] = max (sz[!m_Dir], cs[!m_Dir]);
	sz[m_Dir] += cs[m_Dir];
	tr[0][m_Dir] += cs[m_Dir];
    }
    sz[m_Dir] += m_RMargin - m_Spacing;
    tr[0] = tl;
    tr[1] = tr[0] + sz;
}

//----------------------------------------------------------------------

/// Creates alignment layout with axis \p dir, gravity \p where, and margins.
CAlignLayout::CAlignLayout (ELayoutDir dir, ELayoutAlign where, uint8_t lmargin, uint8_t rmargin)
: CLayout (),
  m_Dir (dir),
  m_Where (where),
  m_LMargin (lmargin),
  m_RMargin (rmargin)
{
}

/// Aligns item using parameters.
void CAlignLayout::OnResize (rcrect_t r)
{
    Rect nr (r);
    SizeHints (nr);
    nr[0][m_Dir] += m_LMargin;
    nr[1][m_Dir] -= m_RMargin;
    LI(0).OnResize (nr);
}

/// Returns the rect of the aligned item.
void CAlignLayout::SizeHints (rrect_t nr) const
{
    Rect cwr (nr);
    LI(0).SizeHints (cwr);
    Size2d sz (cwr.Size());
    sz[m_Dir] += m_LMargin + m_RMargin;
    if (m_Where == la_Center)
	nr[0][m_Dir] += ((nr[1][m_Dir] - nr[0][m_Dir]) - sz[m_Dir]) / 2;
    else if (m_Where == la_Right)
	nr[0][m_Dir] = nr[1][m_Dir] - sz[m_Dir];
    nr[1] = nr[0] + sz;
}

//----------------------------------------------------------------------

/// Resizes item to fit inside the border.
void CBorderLayout::OnResize (rcrect_t r)
{
    if (!NLI())
	return;
    Rect intr (r);
    intr.Expand (-1);
    LI(0).OnResize (intr);
}

/// Returns rect of item with border.
void CBorderLayout::SizeHints (rrect_t intr) const
{
    if (!NLI())
	return;
    intr.Expand (-1);
    LI(0).SizeHints (intr);
    intr.Expand (1);
}

//----------------------------------------------------------------------

