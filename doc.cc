// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// doc.cc
//

#include "doc.h"

//----------------------------------------------------------------------

const CCmdTarget::SCmd CCmdTarget::c_NullCmd = { 0, 0, 0, "" };

//----------------------------------------------------------------------

/// Default constructor.
CDocumentClient::CDocumentClient (void)
: CCmdTarget (),
  m_pDocument (NULL)
{
}

/// Updates the document pointer and calls OnUpdate (or OnInitialUpdate the first time)
void CDocumentClient::Update (pdoc_t pDoc)
{
    assert (pDoc && "If you don't have a document, don't register it.");
    SetFlag (f_Changed, false);
    SetFlag (f_ReadOnly, pDoc->Flag (f_ReadOnly));
    if (m_pDocument != pDoc) {
	m_pDocument = pDoc;
	OnInitialUpdate();
    } else
	OnUpdate();
}

//----------------------------------------------------------------------

/// Default constructor.
CDocument::CDocument (void)
: CDocumentClient (),
  m_Views (),
  m_Filename ()
{
}

/// Registers window \p pView for updates from this document.
void CDocument::RegisterView (pview_t pView)
{
    assert (pView);
    foreach (vwvec_t::const_iterator, i, m_Views)
	if (*i == pView)
	    return;
    m_Views.push_back (pView);
    pView->Update (this);
}

/// Removes window \p pView from the document's update list.
void CDocument::UnregisterView (pview_t pView)
{
    foreach (vwvec_t::iterator, i, m_Views)
	if (*i == pView)
	    --(i = m_Views.erase (i));
}

/// Sends updates to all registered views.
void CDocument::UpdateAllViews (void)
{
    foreach (vwvec_t::iterator, i, m_Views)
	(*i)->Update (this);
}

/// Sets \p filename as the currently open file. Derivatives should load.
void CDocument::Open (rcfname_t filename)
{
    m_Filename = filename;
    SetFlag (f_Changed, false);
}

/// Clears flags and state related to the currently open file.
void CDocument::Close (void)
{
    m_Filename.clear();
    SetFlag (f_Changed, false);
}

