// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// dialog.cc
//

#include "dialog.h"

/// Default constructor.
CDialog::CDialog (void)
{
}

/// Draws the dialog window.
void CDialog::OnDraw (CGC& gc)
{
    gc.Color (black, cyan);
    CWindow::OnDraw (gc);
    gc.Box (WindowArea());
}

/// Processes \p key.
void CDialog::OnKey (wchar_t key)
{
    switch (key) {
	case kv_Tab:	SetFocus (GetNextFocus());	break;
	case kv_Tab | kvm_Shift:
			SetFocus (GetNextFocus(-1));	break;
	default:	CWindow::OnKey (key);		break;
    };
}

/// Closes the dialog when a control closes itself.
/// This allows delegation of command closings to controls.
void CDialog::OnChildClose (uoff_t i)
{
    CWindow::OnChildClose (i);
    Close (CW(i).Status());
}

/// Returns the recommended area of the window when in \p r.
void CDialog::SizeHints (rrect_t wr) const
{
    const Size2d scrsz (wr.Size());
    CWindow::SizeHints (wr);
    wr += (scrsz - wr.Size()) / 2;	// Center the dialog on the screen.
}

