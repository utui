// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// rootwin.h
//

#ifndef ROOTWIN_H_66E7B71D025F65121A9EDB8A49C962B3
#define ROOTWIN_H_66E7B71D025F65121A9EDB8A49C962B3

#include "window.h"

class CDialog;

/// \class CRootWindow rootwin.h utui.h
///
/// This is the one and only root window of an application. It handles
/// combining the output of all its child windows and rendering the result
/// to the terminal. It also manages the event loop, receives and routes
/// user input, and rations idle processing.
///
class CRootWindow : public CWindow {
public:
    static CRootWindow&	Instance (void);
    virtual	       ~CRootWindow (void);
    virtual void	OnCreate (void);
    virtual void	OnDestroy (void);
    virtual void	OnResize (rcrect_t wr);
    void		OnError (exception& e);
    int			Run (void);
    int			RunModal (CDialog* pw);
    static inline void	SetIdleTimeout (long v)		{ s_IdleTimeout = v; }
    static inline void	SetMinIdleTimeout (long v)	{ s_IdleTimeout = min (s_IdleTimeout, v); }
protected:
    void		Flush (void);
    virtual void	OnChildClose (uoff_t i);
private:
			CRootWindow (void);
    wchar_t		GetKey (void) const;
private:
    CTerminfo		m_TI;			///< The terminfo rendering backend object.
    CKeyboard		m_Kb;			///< The terminal keyboard driver.
    CGC			m_Screen;		///< Currently visible screen buffer.
    Point2d		m_ScreenPos;		///< Cursor position on the screen, if any.
    static long		s_IdleTimeout;		///< How often to call OnIdle in absence of keystrokes.
    static int		s_ModalStack [15];	///< Return values of currently active modal dialogs.
    static unsigned int	s_ModalDepth;		///< Depth of the current modal call stack.
};

#endif

