include Config.mk

################ Source files ########################################

CDIRS	= ctrl
SRCS	= $(wildcard *.cc) $(foreach dir, ${CDIRS}, $(wildcard $(dir)/*.cc))
INCS	= $(filter-out bsconf.h, $(wildcard *.h)) $(foreach dir, ${CDIRS}, $(wildcard $(dir)/*.h))
OBJS	= $(SRCS:.cc=.o)
DOCT	= utuidoc.in

################ Library link names ##################################

LIBA	= lib${LIBNAME}.a
LIBSO	= lib${LIBNAME}.so
ifdef MAJOR
LIBSOLNK= ${LIBSO}.${MAJOR}
LIBSOBLD= ${LIBSO}.${MAJOR}.${MINOR}.${BUILD}
endif
TOCLEAN	+= ${LIBSO} ${LIBA} ${LIBSOBLD}

ALLINST	= install-incs
ifdef BUILD_SHARED
ALLLIBS += ${LIBSOBLD}
ALLINST	+= install-shared
endif
ifdef BUILD_STATIC
ALLLIBS += ${LIBA}
ALLINST	+= install-static
endif

################ Compilation #########################################

.PHONY: all install uninstall install-incs uninstall-incs
.PHONY: install-static install-shared uninstall-static uninstall-shared
.PHONY: clean depend html check dist distclean maintainer-clean

all:	${ALLLIBS}

%.o:	%.cc
	@echo "    Compiling $< ..."
	@${CXX} ${CXXFLAGS} -o $@ -c $<

%.s:	%.cc
	@echo "    Compiling $< to assembly ..."
	@${CXX} ${CXXFLAGS} -S -g0 -Os -DNDEBUG=1 -fomit-frame-pointer -o $@ -c $<

html:
	@${DOXYGEN} ${DOCT}

################ Installation ########################################

install:        ${ALLINST}
uninstall:      $(subst install,uninstall,${ALLINST})
	 
${LIBA}:	${OBJS}
	@echo "Linking $@ ..."
	@${AR} r $@ $?
	@${RANLIB} $@

${LIBSOBLD}:	${OBJS}
	@echo "Linking $@ ..."
	@${LD} ${LDFLAGS} ${SHBLDFL} -o $@ $^ ${LIBS}

install-shared: ${LIBSOBLD}
	@echo "Installing ${LIBSOBLD} to ${LIBDIR} ..."
	@${INSTALLDIR} ${LIBDIR}
	@${INSTALLLIB} ${LIBSOBLD} ${LIBDIR}
	@(cd ${LIBDIR}; ln -sf ${LIBSOBLD} ${LIBSO}; ln -sf ${LIBSOBLD} ${LIBSOLNK})

uninstall-shared:
	@echo "Removing ${LIBSOBLD} from ${LIBDIR} ..."
	@rm -f ${LIBDIR}/${LIBSO} ${LIBDIR}/${LIBSOLNK} ${LIBDIR}/${LIBSOBLD}

install-static: ${LIBA}
	@echo "Installing ${LIBA} to ${LIBDIR} ..."
	@${INSTALLDIR} ${LIBDIR}
	@${INSTALLLIB} ${LIBA} ${LIBDIR}

uninstall-static:
	@echo "Removing ${LIBA} from ${LIBDIR} ..."
	@rm -f ${LIBDIR}/${LIBA}

install-incs: ${INCS}
	@echo "Installing headers to ${INCDIR} ..."
	@${INSTALLDIR} ${INCDIR}/${LIBNAME}
	@for i in $(filter-out ${LIBNAME}.h, ${INCS}); do					\
	    ${INSTALLDIR} `dirname ${INCDIR}/${LIBNAME}/$$i`;	\
	    ${INSTALLDATA} $$i ${INCDIR}/${LIBNAME}/$$i;	\
	done;
	@${INSTALLDATA} ${LIBNAME}.h ${INCDIR}

uninstall-incs:
	@echo "Removing headers from ${INCDIR} ..."
	@rm -rf ${INCDIR}/${LIBNAME} ${INCDIR}/${LIBNAME}.h

################ Maintenance ###########################################

clean:
	@echo "Removing generated files ..."
	@rm -f ${OBJS} ${TOCLEAN}

depend: ${SRCS}
	@rm -f .depend
	@for i in ${SRCS}; do	\
	    ${CXX} ${CXXFLAGS} -M -MT $${i%%.cc}.o $$i >> .depend;	\
	done

TMPDIR	= /tmp
DISTDIR	= ${HOME}/stored
DISTNAM	= ${LIBNAME}-${MAJOR}.${MINOR}
DISTTAR	= ${DISTNAM}.${BUILD}.tar.bz2

dist:
	mkdir ${TMPDIR}/${DISTNAM}
	cp -r . ${TMPDIR}/${DISTNAM}
	+make -C ${TMPDIR}/${DISTNAM} html distclean
	(cd ${TMPDIR}/${DISTNAM}; rm -rf `find . -name .svn`)
	(cd ${TMPDIR}; tar jcf ${DISTDIR}/${DISTTAR} ${DISTNAM}; rm -rf ${DISTNAM})

distclean:	clean
	@rm -f Config.mk config.h bsconf.o bsconf .depend

maintainer-clean: distclean
	@rm -rf docs/html

-include .depend
 
