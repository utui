// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// cmdtarg.h
//

#ifndef CMDTARG_H_647A4AD61AAA6CBE021A0E9D7A89F29F
#define CMDTARG_H_647A4AD61AAA6CBE021A0E9D7A89F29F

#include "config.h"
using namespace utio;
using namespace utio::gdt;

class CDocument;

/// \class CCmdTarget cmdtarg.h utui.h
///
/// Target interface for processing commands sent by UI objects like buttons,
/// menu items, etc., and for udpating the visible state of those commands.
///
class CCmdTarget {
public:
    enum { f_Last };
    typedef uint16_t		cmd_t;
    /// Command description object, used in arrays for defining command menus.
    struct SCmd {
	enum EFlags {
	    cf_FocusOnly=(1 << 0),
	    cf_Grayed	=(1 << 1),
	    cf_Checked	=(1 << 2),
	    cf_Submenu	=(1 << 3),
	    cf_Checkable=(1 << 4),
	    cf_Last
	};
	cmd_t		cmd;		///< Command value.
	cmd_t		parent;		///< Command used to trigger the menu containing this command. (As in, File is a parent for Save)
	uint32_t	flags;		///< See EFlags
	char		name [24];	///< Command name as shown in the menu. May contain an & accelerator. Chars in utf8.
	inline void	SetName (const char* n)		{ strncpy (name, n, VectorSize(name)); name[VectorSize(name)-1] = 0; }
	inline void	SetFlag (EFlags f, bool v=true)	{ uint32_t bOn (flags | f), bOff (flags & ~f); flags = v ? bOn : bOff; }
	inline bool	operator== (const SCmd& v) const{ return (*noalias_cast<uint32_t*>(&cmd) == *noalias_cast<uint32_t*>(&v.cmd)); }
	inline bool	operator< (const SCmd& v) const	{ return (cmd < v.cmd); }
    };
    typedef SCmd&	rcmd_t;
    typedef const SCmd&	rccmd_t;
    static const SCmd	c_NullCmd;
public:
    inline		CCmdTarget (void)	: m_Flags(), m_Status (EXIT_SUCCESS) { }
    inline virtual     ~CCmdTarget (void)	{ }
    inline bool		Flag (uoff_t i)	const	{ return (m_Flags[i]); }
    inline int32_t	Status (void) const	{ return (m_Status); }
    inline virtual void	OnCommand (cmd_t)	{ }
    inline virtual void	OnUpdateCommandUI (rcmd_t) const { }
protected:
    inline void		SetFlag (uoff_t i, bool v = true) { m_Flags.set (i, v); }
    inline void		ClearFlag (uoff_t i)	{ SetFlag (i, false); }
    inline void		SetStatus (uint32_t v)	{ m_Status = v; }
private:
    bitset<32>		m_Flags;	///< A bitfield with various values.
    int32_t		m_Status;	///< Last operation status code.
};

//----------------------------------------------------------------------

#define CMD_FULL(text, cmd, flags, nsubs)\
    { cmd, nsubs, flags, text }
#define CMD(text, cmd, flags)	\
    CMD_FULL (text, cmd, flags, 0)

//----------------------------------------------------------------------

#endif

