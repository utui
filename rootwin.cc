// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// rootwin.cc
//

#include "rootwin.h"
#include "msgbox.h"

//----------------------------------------------------------------------

long CRootWindow::s_IdleTimeout = 200000;

//----------------------------------------------------------------------

/// Singleton instance reference.
/*static*/ CRootWindow& CRootWindow::Instance (void)
{
    static CRootWindow w;
    return (w);
}

/// Default constructor.
CRootWindow::CRootWindow (void)
: CWindow (),
  m_TI (),
  m_Kb (),
  m_Screen (),
  m_ScreenPos ()
{
}

/// Virtual destructor.
CRootWindow::~CRootWindow (void)
{
    CRootWindow::OnDestroy();	// This is for abnormal exit.
}

/// Writes the window GC onto the terminal.
void CRootWindow::Flush (void)
{
    CGC& rgc (GC());
    bool bCursorOff = (m_ScreenPos == pos_CaretOff);
    bool bWillBeOff = (CaretPos() == pos_CaretOff);
    if (rgc.MakeDiffFrom (m_Screen)) {
	// gc now has only new stuff, the rest is zeroed out, and isn't drawn.
	cout << m_TI.HideCursor();
	bCursorOff = true;
	cout << m_TI.Image (0, 0, rgc.Width(), rgc.Height(), rgc.Canvas().begin());
	m_Screen.Image (rgc);      // Now apply the same diff to the screen cache.
	rgc.Image (m_Screen);      // ... and copy it back for a fresh start.
    }
    if (!bCursorOff && bWillBeOff) {
	cout << m_TI.HideCursor();
	bCursorOff = true;
    }
    bCursorOff |= (m_ScreenPos != CaretPos());
    if (bCursorOff & ((m_ScreenPos = CaretPos()) != pos_CaretOff)) {
	cout << m_TI.MoveTo (CaretPos()[0], CaretPos()[1]);
	cout << m_TI.ShowCursor();
    }
    if (bCursorOff)
	cout.flush();
}

/// Initializes the output device.
void CRootWindow::OnCreate (void)
{
    CWindow::OnCreate();

    m_TI.Load();		// The terminfo database
    m_Kb.Open (m_TI);		// Put the keyboard in UI mode
    cout << m_TI.HideCursor();	// No cursor in UI mode
    cout << m_TI.Clear();	// Screen is clear in the beginning

    OnResize (Rect (0, 0, m_TI.Width(), m_TI.Height()));
}

/// Deinitializes the output device.
void CRootWindow::OnDestroy (void)
{
    if (!m_Kb.IsInUIMode())	// To not close twice (from dtor)
	return;
    m_Kb.Close();		// Coming back to tty mode
    cout << m_TI.Clear();	// Clear the screen
    cout << m_TI.ShowCursor();	// No cursor in UI mode
    cout.flush();		// Now
    CWindow::OnDestroy();	// OnDestroy errors can now be printed to tty
}

/// Sets the window rect to \p wr.
void CRootWindow::OnResize (rcrect_t wr)
{
    CWindow::OnResize (wr);
    m_Screen.Resize (wr.Width(), wr.Height());
    // Root window children are placed wherever they want to be.
    Rect cwr;
    foreach (winvec_t::iterator, i, Children()) {
	(*i)->SizeHints (cwr = wr);
	(*i)->OnResize (cwr);
    }
}

/// Called when a root-level exception is caught.
void CRootWindow::OnError (exception& e)
{
    // Print the exception to string
    ostringstream os;
    os << e;
    // Delete all windows to avoid further errors.
    DeleteAllChildren();
    // Display the error in a box.
    MessageBox (os.str());
    // If any of the above throws, Terminate gets called with a message and a dtor call.
}

/// Checks for keypresses and other events.
wchar_t CRootWindow::GetKey (void) const
{
    wchar_t key = m_Kb.GetKey (false);
    if (!key)
	m_Kb.WaitForKeyData (s_IdleTimeout);
    return (key);
}

/// The main event loop.
int CRootWindow::Run (void)
{
    OnGainFocus();
    while (!Flag (f_Closed) & !Children().empty()) {
	Paint();
	Flush();
	wchar_t key;
	if ((key = GetKey()))
	    OnKey (key);
	OnIdle();
    }
    OnLoseFocus();
    return (Status());
}

//----------------------------------------------------------------------

int CRootWindow::s_ModalStack [15];
unsigned int CRootWindow::s_ModalDepth = 1;

/// Extracts status information from the child window before it dies.
void CRootWindow::OnChildClose (uoff_t i)
{
    CWindow::OnChildClose (i);
    s_ModalStack[s_ModalDepth-1] = CW(i).Status();
}

/// Runs \p pw as a modal dialog. \p pw is deleted before returning.
int CRootWindow::RunModal (CDialog* pw)
{
    assert (s_ModalDepth < VectorSize(s_ModalStack) - 1 && "Too many nested modal dialog calls. Either use fewer or increase s_ModalStack size in rootwin.cc");
    s_ModalStack [s_ModalDepth++] = 0;
    uint32_t ci = Children().size(), oldFocus (Focus());
    AddChild (pw);
    SetFocus (ci);
    while ((ci < Children().size()) && Children()[ci] == pw) {
	Paint();
	Flush();
	wchar_t key;
	if ((key = GetKey()))
	    pw->OnKey (key);
	OnIdle();
    }
    if (oldFocus >= Children().size())
	oldFocus = GetNextFocus(-1);
    SetFocus (oldFocus);
    return (s_ModalStack [--s_ModalDepth]);
}

