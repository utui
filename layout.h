// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// layout.h
//

#ifndef LAYOUT_H_75D6286B51F3FD37071DA7E92C97A86E
#define LAYOUT_H_75D6286B51F3FD37071DA7E92C97A86E

#include "autowig.h"

//----------------------------------------------------------------------

/// Specifies layout axis.
enum ELayoutDir {
    ld_Horizontal,
    ld_Vertical
};

/// Specifies gravity for alignment.
enum ELayoutAlign {
    la_Left,
    la_Top = la_Left,
    la_Center,
    la_Right,
    la_Bottom = la_Right
};

//----------------------------------------------------------------------

/// \class CLayout layout.h layout.h
///
/// \brief Helps windows layout children.
///
/// Each CLayout maintains a non-owning list of other layout items, which may
/// be of type CLayout or \ref CWindow. Rectangles passed to layout items for
/// measurement and resizing are not relative to this object's rect. This can
/// allow nesting of CLayout items.
///
class CLayout {
public:
    typedef vector<CLayout*>	itemvec_t;
    typedef const Rect&		rcrect_t;
    typedef Rect&		rrect_t;
    typedef const Point2d&	rcpos_t;
public:
			CLayout (void);
    virtual	       ~CLayout (void);
    inline void		AddLayoutItem (CLayout* pi)	{ m_Items.push_back (pi); }
    virtual void	OnResize (rcrect_t r);
    virtual void	SizeHints (rrect_t wr) const;
protected:
    inline size_t	NLI (void) const	{ return (m_Items.size()); }
    inline CLayout&	LI (uoff_t i)		{ return (*m_Items[i]); }
  inline const CLayout&	LI (uoff_t i) const	{ return (*m_Items[i]); }
    void		DeleteAllLayoutItems (void);
private:
    itemvec_t		m_Items;	///< Vector of all the items to be laid out (non-owning)
};

//----------------------------------------------------------------------

/// \class CTileLayout layout.h layout.h
/// \brief Tiles items along a single axis.
class CTileLayout : public CLayout {
public:
			CTileLayout (ELayoutDir dir = ld_Horizontal, uint8_t spacing = 1, uint8_t lmargin = 0, uint8_t rmargin = 0);
protected:
    virtual void	OnResize (rcrect_t r);
    virtual void	SizeHints (rrect_t wr) const;
private:
    uint8_t		m_Dir;		///< Alignment axis direction. See \ref ELayoutDir
    uint8_t		m_Spacing;	///< Interitem spacing.
    uint8_t		m_LMargin;	///< Margin to add at the beginning of the axis.
    uint8_t		m_RMargin;	///< Margin to add at the end of the axis.
};

#define LAYOUT_TILE(ncw, dir, spacing, lmargin, rmargin)	\
	{ ncw, &TWidgetFactory<CTileLayout>, WIDIVAL_AB(dir, spacing, lmargin, rmargin) },
template <> inline pwidget_t TWidgetFactory<CTileLayout> (widival_t v)
    { return (new CTileLayout (ELayoutDir(v.ab[0]), v.ab[1], v.ab[2], v.ab[3])); }

//----------------------------------------------------------------------

/// \class CAlignLayout layout.h layout.h
/// \brief Aligns one item left, center, or right along one axis.
class CAlignLayout : public CLayout {
public:
			CAlignLayout (ELayoutDir dir = ld_Horizontal, ELayoutAlign where = la_Left, uint8_t lmargin = 0, uint8_t rmargin = 0);
protected:
    virtual void	OnResize (rcrect_t r);
    virtual void	SizeHints (rrect_t wr) const;
private:
    uint8_t		m_Dir;		///< Alignment axis direction. See \ref ELayoutDir
    uint8_t		m_Where;	///< Type of alignment to perform. See \ref ELayoutAlign
    uint8_t		m_LMargin;	///< Margin to add at the beginning of the axis.
    uint8_t		m_RMargin;	///< Margin to add at the end of the axis.
};

#define LAYOUT_ALIGN(ncw, dir, where, lmargin, rmargin)	\
	{ ncw, &TWidgetFactory<CAlignLayout>, WIDIVAL_AB(dir, where, lmargin, rmargin) },
template <> inline pwidget_t TWidgetFactory<CAlignLayout> (widival_t v)
    { return (new CAlignLayout (ELayoutDir(v.ab[0]), ELayoutAlign(v.ab[1]), v.ab[2], v.ab[3])); }

//----------------------------------------------------------------------

/// \class CBorderLayout layout.h layout.h
/// \brief Embeds one item inside a border of width 1.
class CBorderLayout : public CLayout {
protected:
    virtual void	OnResize (rcrect_t r);
    virtual void	SizeHints (rrect_t wr) const;
};

#define LAYOUT_BORDER(ncw)	\
	{ ncw, &TWidgetFactory<CBorderLayout>, {0} },
template <> inline pwidget_t TWidgetFactory<CBorderLayout> (widival_t)
    { return (new CBorderLayout); }

//----------------------------------------------------------------------

#endif
