// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// window.h
//

#ifndef WiNDOW_H_2E2B19315923E2C14BED26747E7C832B
#define WiNDOW_H_2E2B19315923E2C14BED26747E7C832B

#include "layout.h"

#define NO_FOCUS	UINT32_MAX

/// \class CWindow window.h utui.h
///
/// A window object. Derive your window classes from this to obtain output
/// and event-handling capabilities. Each window may have child windows of
/// its own, which can be laid out manually or by use of layout objects.
///
class CWindow : public CDocumentClient, public CLayout {
public:
    typedef CWindow*		pwin_t;
    typedef const CWindow*	pcwin_t;
public:
			CWindow (void);
    virtual	       ~CWindow (void);
    virtual void	OnCreate (void);
    virtual void	OnDestroy (void);
    virtual void	OnKey (wchar_t);
    virtual void	OnCommand (cmd_t cmd);
    virtual void	OnUpdateCommandUI (rcmd_t cmd) const;
    virtual void	OnResize (rcrect_t wr);
    virtual void	SizeHints (rrect_t wr) const;
    inline rcrect_t	WindowRect (void) const	{ return (m_WindowRect); }
    void		AddChild (pwin_t pw);
    void		RemoveChild (pwin_t pw);
    void		DeleteAllChildren (void);
    inline void		Close (void)		{ SetFlag (f_Closed); }
    inline rcpos_t 	CaretPos (void) const	{ return (m_CaretPos); }
    void		Paint (void);
    bool		SeenCWRect (Rect& r, pcwin_t pw) const;
    pcwidesc_t		InitFromDesc (pcwidesc_t pwd, pwidget_t parent = NULL);
    pwin_t		ParentWindowOf (pcwin_t w);
protected:
    typedef vector<pwin_t>	winvec_t;
    typedef winvec_t&		rwinvec_t;
    typedef const winvec_t&	rcwinvec_t;
    typedef CWindow&		rwin_t;
    typedef const CWindow&	rcwin_t;
    /// Miscellaneous flags; pass to SetFlag or Flag
    enum EFlags {
	f_Created = CDocumentClient::f_Last,	///< OnCreate has been called.
	f_Resized,		///< OnResize has been called.
	f_HasFocus,		///< Has input focus.
	f_OffersFocus,		///< Doesn't want the focus any more.
	f_NoTabOrder,		///< Can't be focused by the user.
	f_Closed,		///< Should be closed by parent.
	f_Last
    };
    /// Command key table element
    struct SCommandKey {
	wchar_t	key;		///< The key.
	wchar_t	cmd;		///< The command it invokes (from the list in CMenuBar)
    };
    typedef vector<SCommandKey>	cmdkvec_t;
    static const Point2d	pos_CaretOff;
protected:
    inline rwinvec_t	Children (void)		{ return (m_Children); }
    inline rcwinvec_t	Children (void) const	{ return (m_Children); }
    inline rwin_t	CW (uoff_t i)		{ return (*m_Children[i]); }
    inline rcwin_t	CW (uoff_t i) const	{ return (*m_Children[i]); }
    template <typename T>
    inline T&		TCW (uoff_t i)		{ return (static_cast<T&>(CW(i))); }
    template <typename T>
    inline const T&	TCW (uoff_t i) const	{ return (static_cast<const T&>(CW(i))); }
    inline CGC&		GC (void)		{ return (m_GC); }
    inline const CGC&	GC (void) const		{ return (m_GC); }
    inline Rect		WindowArea (void) const	{ return (WindowRect() - WindowRect()[0]); }
    inline void		SetCommandKeys (const SCommandKey* pCmds, size_t nCmds);
    inline void		AddCommandKey (wchar_t key, wchar_t cmd);
    inline bool		HasFocus (void) const	{ return (Flag (f_HasFocus)); }
    inline uint32_t	Focus (void) const	{ return (m_Focus); }
    void		SetFocus (uoff_t f);
    uint32_t		GetNextFocus (int dir = 1) const;
    inline virtual void	OnDraw (CGC& gc)	{ gc.Clear(); }
    void		CaretTo (rcpos_t pt);
    inline void		CaretTo (coord_t x, coord_t y)	{ CaretTo (Point2d (x, y)); }
    inline void		CaretOff (void)			{ CaretTo (pos_CaretOff); }
    pwin_t		ParentWindow (void) const;
    inline void		Close (int32_t code)		{ SetStatus (code); Close(); }
    virtual void	OnGainFocus (void);
    virtual void	OnLoseFocus (void);
    virtual void	OnIdle (void);
    virtual void	OnChildClose (uoff_t i);
 inline virtual wchar_t	FocusAccelKey (void) const { return (0); }
private:
    CGC			m_GC;		///< The GC into which to draw.
    winvec_t		m_Children;	///< Child window list.
    cmdkvec_t		m_CommandKeys;	///< Command keys
    Rect		m_WindowRect;	///< Window location on the screen.
    uint32_t		m_Focus;	///< Index of the focused child window.
    Point2d		m_CaretPos;	///< Position of the caret in the window.
};

//----------------------------------------------------------------------

/// Values ORed onto a key value received by OnKey.
enum EKeyModifier {
    kvm_FirstModBit = 24,
    kvm_First	= (1 << kvm_FirstModBit),
    kvm_KeyMask	= kvm_First - 1,
    kvm_ModMask = ~kvm_KeyMask,
    //
    kvm_Shift	= kvm_First,
    kvm_Alt	= kvm_First << 1,
    kvm_Ctrl	= kvm_First << 2,
    kvm_Meta	= kvm_First << 3,
    kvm_Command	= kvm_First << 4
};

//----------------------------------------------------------------------

/// Used to set key-command mappings for this window.
///
/// Create a static array of SCommandKeys and pass it here, which links
/// an internal vector to it. That vector is then automatically processed
/// in OnKey, dispatching commands as needed. Ensure pCmds continues its
/// existence until the window is destroyed.
///
inline void CWindow::SetCommandKeys (const SCommandKey* pCmds, size_t nCmds)
    { m_CommandKeys.link (pCmds, nCmds); }

inline void CWindow::AddCommandKey (wchar_t key, wchar_t cmd)
{
    m_CommandKeys.push_back();
    m_CommandKeys.back().key = key;
    m_CommandKeys.back().cmd = cmd;
}

//----------------------------------------------------------------------

#endif

