// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// doc.h
//

#ifndef DOC_H_7BBE3FAF41D4C26A3BB1D8E8391176E8
#define DOC_H_7BBE3FAF41D4C26A3BB1D8E8391176E8

#include "doccli.h"

/// \class CDocument doc.h doc.h
///
/// Documents are the data storage side of the document-view architecture.
/// Any window can serve as a view to display the data it obtains by using
/// accessor functions of objects derived from CDocument through the pointer
/// sent to it in OnUpdate.
///
class CDocument : public CDocumentClient {
public:
    typedef const string&	rcfname_t;
    typedef CDocumentClient*	pview_t;
public:
			CDocument (void);
    inline virtual     ~CDocument (void)	{ }
    void		RegisterView (pview_t pView);
    void		UnregisterView (pview_t pView);
    virtual void	Open (rcfname_t filename);
    virtual void	Close (void);
    inline rcfname_t	Filename (void) const	{ return (m_Filename); }
    inline virtual void	read (istream&)		{ }
    inline virtual void	write (ostream&) const	{ }
    inline virtual size_t stream_size (void) const { return (0); }
protected:
    void		UpdateAllViews (void);
private:
    typedef vector<pview_t>	vwvec_t;
private:
    vwvec_t		m_Views;	///< Registered views.
    string		m_Filename;	///< Name of the currently open file.
};

STD_STREAMABLE (CDocument)

#endif

