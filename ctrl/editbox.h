// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// editbox.h
//

#ifndef EDITBOX_H_588F8A534212610C6BC064C42EDE2108
#define EDITBOX_H_588F8A534212610C6BC064C42EDE2108

#include "../window.h"

/// \class CEditBox ctrl/editbox.h utui.h
class CEditBox : public CWindow {
public:
			CEditBox (void);
    void		SetText (const string& s);
   inline const string&	Text (void) const		{ return (m_Text); }
    virtual void	OnDraw (CGC& gc);
    virtual void	OnKey (wchar_t key);
private:
    string		m_Text;		///< The text being edited.
    string		m_Original;	///< The unmodified text.
    uoff_t		m_Pos;		///< Current cursor position.
};

#endif

