// Copyright (c) 2006 by Mike Sharov <msharov@users.sourceforge.net>
//
// menu.h
//

#ifndef MENU_H_5D85214F09E197BD553E7F2141F2AA44
#define MENU_H_5D85214F09E197BD553E7F2141F2AA44

#include "../dialog.h"

//----------------------------------------------------------------------

/// List of standard commands
enum {
    cmd_Root,
    cmd_Nop,
    cmd_Separator = cmd_Nop,
    cmd_ExitMenu,
    cmd_NextMenu,
    cmd_PrevMenu,
    cmd_ActionBase = 0x10,
    cmd_File,
    cmd_File_Save,
    cmd_File_Quit,
    cmd_Help,
    cmd_Help_Contents,
    cmd_Help_About,
    cmd_User = 0x100,
};

//----------------------------------------------------------------------

/// \class CMenu ctrl/menu.h utui.h
///
/// Displays a vertical menu of items initialized from the given list,
/// updated via the OnUpdateCommandUI interface on the window tree.
///
class CMenu : public CDialog {
public:
    typedef CCmdTarget*		ptarget_t;
    typedef vector<SCmd>	cmdvec_t;
    typedef const cmdvec_t&	rccmdvec_t;
    typedef cmdvec_t::iterator	iitem_t;
    typedef cmdvec_t::const_iterator	icitem_t;
    typedef const Point2d&	rcoffset_t;
    static const Point2d	s_NullOffset;
public:
    static cmd_t	Run (const SCmd* pCmds, size_t nCmds, cmd_t root = cmd_Root, rcoffset_t offset = s_NullOffset);
    void		Set (const SCmd* pCmds, size_t nCmds, cmd_t root = cmd_Root);
    SCmd*		ConvertCountToParent (SCmd* pCmd, const SCmd* pCmdEnd, cmd_t parentCmd) const;
    cmd_t		ExecuteCommand (cmd_t cmd);
protected:
			CMenu (const SCmd* pCmds = NULL, size_t nCmds = 0, cmd_t root = cmd_Root, rcoffset_t offset = s_NullOffset);
    virtual void	OnKey (wchar_t key);
    virtual void	OnCommand (cmd_t cmd);
    virtual void	OnUpdateCommandUI (rcmd_t cmd) const;
    virtual void	SizeHints (rrect_t wr) const;
    inline bool		IsEmpty (void) const	{ return (!m_nCmds); }
    inline bool		IsTopBar (void) const	{ return (m_Root == cmd_Root); }
private:
    inline Point2d	SubmenuOffset (cmd_t cmd) const;
    uoff_t		FindItem (cmd_t cmd) const;
private:
    const SCmd*		m_Cmds;		///< List of all commands in this menu hierarchy.
    size_t		m_nCmds;	///< Number of commands in m_Cmds.
    Point2d		m_ScrOffset;	///< Screen offset to place the menu under a specific item.
    cmd_t		m_Root;		///< Parent command to select commands by. See #CWindow::SCmd.
};

//----------------------------------------------------------------------

/// \class CMenuBar ctrl/menu.h utui.h
/// Special case of the root menu, which is the standard menu bar on top of the screen.
class CMenuBar : public CMenu {
public:
    static CMenuBar*	Instance (void);
    void		Set (const SCmd* pCmds, size_t nCmds);
    inline void		ExecuteCommand (cmd_t cmd)	{ OnCommand (cmd); }
protected:
    virtual void	OnKey (wchar_t key);
    virtual void	SizeHints (rrect_t wr) const;
private:
			CMenuBar (void);
    inline wchar_t	CheckCuritemStatus (void);
private:
    vector<SCmd>	m_Cmdv;
};

//----------------------------------------------------------------------

#define BEGIN_MENU(n, name)	\
static const CTodoList::SCmd c_TodoMenu[] = {	\
    SUBMENU(n,"menuroot",cmd_Root),
#define END_MENU		\
};
#define MENUITEM(text, cmd)	\
    CMD_FULL (text, cmd, 0, 0)
#define MENUITEM_SEPARATOR	\
    CMD_FULL ("", cmd_Separator, 0, 0)
#define MENUCHECK(text, cmd)	\
    CMD_FULL (text, cmd, CCmdTarget::SCmd::cf_Checkable, 0)
#define SUBMENU(n, text, cmd)	\
    CMD_FULL (text, cmd, CCmdTarget::SCmd::cf_Submenu, n)

//----------------------------------------------------------------------

#endif
