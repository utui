// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// label.h
//

#ifndef LABEL_H_6E49E50D19E9510D73F9D5DE24DDCDFE
#define LABEL_H_6E49E50D19E9510D73F9D5DE24DDCDFE

#include "../window.h"

/// \class CLabel ctrl/label.h utui.h
/// Displays a static, non-focusable label.
class CLabel : public CWindow {
public:
    typedef const string&	rctext_t;
    typedef const EColor*	ptextcols_t;
public:
			CLabel (rctext_t s = string::empty_string);
    static pwidget_t	Factory (widival_t v);
    inline void		SetText (rctext_t s)	{ m_Text = s; }
    inline rctext_t	Text (void) const	{ return (m_Text); }
protected:
    virtual void	OnDraw (CGC& gc);
    virtual void	SizeHints (rrect_t wr) const;
    virtual wchar_t	FocusAccelKey (void) const;
    coord_t		DrawTextWithAccel (CGC& gc, coord_t x, coord_t y, rctext_t s, ptextcols_t col) const;
    size_t		TextWithAccelSize (rctext_t s) const;
private:
    string		m_Text;	///< Text displayed by the label.
};

#define LABEL(text)		\
	{ 0, &TWidgetFactory<CLabel>, WIDIVAL_TEXT(text) },
template <> inline pwidget_t TWidgetFactory<CLabel> (widival_t v)
    { return (new CLabel (v.text)); }

#endif

