// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// listbox.h
//

#ifndef LISTBOX_H_6E9B307D2D8A2AEB3DF700ED1872B0D1
#define LISTBOX_H_6E9B307D2D8A2AEB3DF700ED1872B0D1

#include "../window.h"

/// \class CListbox ctrl/listbox.h utui.h
/// Generic listbox window with a selection bar.
class CListbox : public CWindow {
public:
			CListbox (void);
    virtual void	OnDraw (CGC& gc);
    virtual void	OnKey (wchar_t key);
    inline void		SetListSize (uint32_t sz)	{ m_ListSize = sz; }
    inline uint32_t	Selection (void) const		{ return (m_Selection); }
    void		SetSelection (uint32_t i);
protected:
    inline uint32_t	TopVisible (void) const		{ return (m_TopVisible); }
    inline virtual void	OnDrawItem (CGC&, rcpos_t, uint32_t)	{ }
private:
    uint32_t		m_Selection;	///< Selected entry index.
    uint32_t		m_TopVisible;	///< Index of the top visible entry.
    uint32_t		m_ListSize;	///< Number of entries in the list.
};

#endif

