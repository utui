// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// button.h
//

#ifndef BUTTON_H_48DE165D79910AEC0122F52549697949
#define BUTTON_H_48DE165D79910AEC0122F52549697949

#include "label.h"

/// \class CButton ctrl/button.h utui.h
/// A pushable button.
class CButton : public CLabel {
public:
			CButton (rccmd_t cmd);
    virtual void	UpdateCmd (void);
    inline rccmd_t	Cmd (void) const		{ return (m_Cmd); }
protected:
    virtual void	OnDraw (CGC& gc);
    virtual void	SizeHints (rrect_t wr) const;
    virtual void	OnKey (wchar_t key);
    inline rcmd_t	Cmd (void)			{ return (m_Cmd); }
    inline bool		CmdFlag (uint32_t f) const	{ return (m_Cmd.flags & f); }
private:
    SCmd		m_Cmd;	///< Command to execute when the button is pressed.
};

#define BUTTON(cmd)	\
	{ 0, &TWidgetFactory<CButton>, WIDIVAL_PTR(static_cast<const CButton::SCmd*>(&cmd)) },
template <> inline pwidget_t TWidgetFactory<CButton> (widival_t v)
    { return (new CButton (*(const CCmdTarget::SCmd*)(v.p))); }

#endif

