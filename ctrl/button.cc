// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// button.cc
//

#include "button.h"

/// Default constructor.
CButton::CButton (const SCmd& cmd)
: CLabel (cmd.name),
  m_Cmd (cmd)
{
    SetFlag (f_NoTabOrder, false);
}

/// Draws the label in the window.
void CButton::OnDraw (CGC& gc)
{
    static const EColor color[2][2][3] = {
	{ { black, green, red },	{ darkgray, cyan, black } },
	{ { white, blue, yellow }, 	{ lightgray, blue, darkgray } }
    };
    UpdateCmd();
    ptextcols_t pcols = color[HasFocus()][CmdFlag(SCmd::cf_Grayed)];
    gc.Color (pcols[0], pcols[1]);
    CWindow::OnDraw (gc);
    gc.Char (0, 0, '[');
    DrawTextWithAccel (gc, 2, 0, Text(), pcols);
    gc.Char (WindowRect().Width() - 1, 0, ']');
}

/// Returns the recommended window area \p br when in \p r.
void CButton::SizeHints (rrect_t br) const
{
    Point2d brbr (br[1]);
    br[1] = br[0];
    br[1][0] += 4 + TextWithAccelSize (Text());
    br[1][1] += 1;
    simd::pmin (brbr, br[1]);
}

/// Processes \p key.
void CButton::OnKey (wchar_t key)
{
    if (key != kv_Enter)
	return (CLabel::OnKey (key));
    UpdateCmd();
    if (!CmdFlag (SCmd::cf_Grayed))
	ParentWindow()->OnCommand (Cmd().cmd);
}

/// Calls OnUpdateCommandUI to update the command flags.
void CButton::UpdateCmd (void)
{
    ParentWindow()->OnUpdateCommandUI (m_Cmd);
    const bool bGray = CmdFlag (SCmd::cf_Grayed);
    SetFlag (f_OffersFocus, Flag(f_OffersFocus) | (Flag(f_HasFocus) & bGray));
    SetFlag (f_NoTabOrder, bGray);
}

