// Copyright (c) 2006 by Mike Sharov <msharov@users.sourceforge.net>
//
// menu.cc
//

#include "menu.h"
#include "menuitem.h"
#include "../rootwin.h"

//----------------------------------------------------------------------

const Point2d CMenu::s_NullOffset;

//----------------------------------------------------------------------

/*static*/ CMenu::cmd_t CMenu::Run (const SCmd* pCmds, size_t nCmds, cmd_t root, rcoffset_t offset)
{
    return (CRootWindow::Instance().RunModal (new CMenu (pCmds, nCmds, root, offset)));
}

//----------------------------------------------------------------------

/// Default constructor.
CMenu::CMenu (const SCmd* pCmds, size_t nCmds, cmd_t root, rcoffset_t offset)
: CDialog (),
  m_Cmds (pCmds),
  m_nCmds (nCmds),
  m_ScrOffset (offset),
  m_Root (root)
{
    assert (pCmds || !nCmds);
    assert (nCmds < UINT16_MAX && "Too many entries in menu!");
    if (pCmds)
	Set (pCmds, nCmds, root);
}

/// Finds item with \p cmd.
uoff_t CMenu::FindItem (cmd_t cmd) const
{
    uoff_t i = 0;
    for (; i < m_nCmds && m_Cmds[i].cmd != cmd; ++i);
    return (i);
}

/// Sets the menu item list to \p pCmds of size \p nCmds with \p root.
/// Commands should be in a hierarchy, as visible.
///
void CMenu::Set (const SCmd* pCmds, size_t nCmds, cmd_t root)
{
    m_Cmds = pCmds;
    m_nCmds = nCmds;
    m_Root = root;
    const bool bTopBar (IsTopBar());
    CTileLayout* pTile (new CTileLayout (bTopBar ? ld_Horizontal : ld_Vertical, 0));
    if (!bTopBar) {
	AddLayoutItem (new CBorderLayout);
	LI(0).AddLayoutItem (pTile);
    }
    AddLayoutItem (pTile);
    for (uoff_t i = 0; i < m_nCmds; ++i) {
	if (m_Cmds[i].parent != m_Root)
	    continue;
	AddChild (new CMenuItem (m_Cmds[i]));
	pTile->AddLayoutItem (Children().back());
    }
}

/// Recursively converts the subcount format to parent index format.
CMenu::SCmd* CMenu::ConvertCountToParent (SCmd* pCmd, const SCmd* pCmdEnd, cmd_t parentCmd) const
{
    assert (pCmd < pCmdEnd && "Your menu definition array is invalid. You must use the MENU macros to define it!");
    const size_t nSubs (pCmd->parent);
    const cmd_t cmdId (pCmd->cmd);
    pCmd->parent = parentCmd;
    ++pCmd;
    for (uoff_t i = 0; i < nSubs; ++i)
	pCmd = ConvertCountToParent (pCmd, pCmdEnd, cmdId);
    return (pCmd);
}

/// Returns recommended window area when in \p r.
void CMenu::SizeHints (rrect_t mr) const
{
    Point2d mrbr (mr[1]);
    CWindow::SizeHints (mr);
    mr += m_ScrOffset;		// To open next to the parent item
    simd::pmin (mrbr, mr[1]);	// Clip
}

/// Handles \p key.
void CMenu::OnKey (wchar_t key)
{
    switch (key) {
	case kv_Esc:	Close (cmd_ExitMenu);		break;
	case kv_Up:	SetFocus (GetNextFocus (-1));	break;
	case kv_Down:	SetFocus (GetNextFocus());	break;
	case kv_Left:	Close (cmd_PrevMenu);		break;
	case kv_Right:	if (!(TCW<const CMenuItem>(Focus()).Cmd().flags & SCmd::cf_Submenu)) {
			    Close (cmd_NextMenu);
			    break;
			}
	case kv_Enter:	CDialog::OnKey (kv_Enter); {
			int rv = CW(Focus()).Status();
			if (rv == cmd_ExitMenu || rv >= cmd_ActionBase)
			    Close (rv);
			break; }
	default:	CDialog::OnKey (key);		break;
    }
}

/// Returns submenu placement point in root coordinates.
inline Point2d CMenu::SubmenuOffset (cmd_t cmd) const
{
    Rect wr;
    uoff_t ic = 0;
    // Submenu offset is available only for direct children of the menu.
    // Non-related submenu items should be opened in the top-left corner.
    for (ic = 0; ic < Children().size() && TCW<CMenuItem>(ic).Cmd().cmd != cmd; ++ic);
    if (ic < Children().size())
	CRootWindow::Instance().SeenCWRect (wr, &CW(ic));
    const bool bTopBar = IsTopBar();
    wr[0][0] = wr[!bTopBar][0];
    wr[0][1] += bTopBar - !bTopBar;
    return (wr[0]);
}

/// Processes command \p cmd.
void CMenu::OnCommand (cmd_t cmd)
{
    // Find the command in the command list.
    uoff_t ii = FindItem (cmd);
    if (ii >= m_nCmds)
	return; // Commands are global and some might not be on this menu's list.
    // Check if this is a submenu item
    SCmd cmdo (m_Cmds[ii]);
    CRootWindow::Instance().OnUpdateCommandUI (cmdo);
    if (cmdo.flags & SCmd::cf_Grayed)
	return;
    else if (cmdo.flags & SCmd::cf_Submenu) {
	cmd_t rv = Run (m_Cmds, m_nCmds, cmdo.cmd, SubmenuOffset(cmdo.cmd));
	// Submenu may tell us to close or to move around
	if (rv < cmd_ExitMenu)
	    return;
	if (!IsTopBar())
	    return (Close (rv));
	if (rv == cmd_NextMenu)
	    OnKey (kv_Right);
	else if (rv == cmd_PrevMenu)
	    OnKey (kv_Left);
	else
	    OnKey (kv_Esc);
    } else if (cmdo.cmd >= cmd_ActionBase) {
	static int bInHere = false;	// Yeah, it's a bit weird, but it works.
	if (!TestAndSet (&bInHere)) return;
	CRootWindow::Instance().OnCommand (cmdo.cmd);
	if (!IsTopBar())
	    Close (cmdo.cmd);
	bInHere = false;	// Root window OnCommand calls this OnCommand.
    }				// Just accept you don't understand it ;) And don't mess with it.
}

/// Updates \p cmd information.
void CMenu::OnUpdateCommandUI (rcmd_t cmd) const
{
    static int bInHere = false;
    if (!TestAndSet (&bInHere)) return;
    CRootWindow::Instance().OnUpdateCommandUI (cmd);
    bInHere = false;
}

//----------------------------------------------------------------------

/// Singleton instance.
/*static*/ CMenuBar* CMenuBar::Instance (void)
{
    static CMenuBar* pBar = new CMenuBar;	// Deleted by the frame.
    return (pBar);
}

//----------------------------------------------------------------------

/// Default constructor.
CMenuBar::CMenuBar (void)
: CMenu()
{
    SetFlag (f_NoTabOrder);
}

void CMenuBar::Set (const SCmd* pCmds, size_t nCmds)
{
    m_Cmdv.link (pCmds, nCmds);
    m_Cmdv.copy_link();
#ifndef NDEBUG
    SCmd* pCmdvEnd =
#endif
    ConvertCountToParent (m_Cmdv.begin(), m_Cmdv.end(), UINT16_MAX);
    assert (pCmdvEnd == m_Cmdv.end() && "You have an incorrect item count in one of the submenus in your menu definition array.");
    CMenu::Set (m_Cmdv.begin(), m_Cmdv.size(), cmd_Root);
}

/// Returns recommended window area when in \p r.
void CMenuBar::SizeHints (rrect_t wr) const
{
    wr -= wr[0];
    wr[1][1] = !IsEmpty(); // MenuBar is a line on top.
}

/// Executes the currently focused item and returns key action.
inline wchar_t CMenuBar::CheckCuritemStatus (void)
{
    static const uint16_t cmdkey[] = { 0, 0, kv_Esc, kv_Right, kv_Left, kv_Esc };	// Keep in sync with cmd enum in menuitem.h
    return (cmdkey [min (uoff_t(CW(Focus()).Status()), VectorSize(cmdkey)-1)]);
}

/// Handles \p key.
void CMenuBar::OnKey (wchar_t key)
{
again:
    switch (key) {
	case kv_Left:	SetFocus (GetNextFocus (-1));
			key = kv_Enter;			goto again;
	case kv_Right:	SetFocus (GetNextFocus());
			key = kv_Enter;			goto again;
	case kv_Esc:
	case kv_Up:	SetFlag (f_OffersFocus);	break;
	default:	CDialog::OnKey (key);
			if (!(key = CheckCuritemStatus())) break;
			goto again;
    }
}

