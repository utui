// Copyright (c) 2006 by Mike Sharov <msharov@users.sourceforge.net>
//
// menuitem.cc
//

#include "menu.h"
#include "menuitem.h"

/// Draws the menu item.
void CMenuItem::OnDraw (CGC& gc)
{
    static const EColor color[2][2][3] = {
	{ { black, cyan, red },		{ darkgray, cyan, black } },
	{ { white, blue, yellow }, 	{ lightgray, blue, darkgray } }
    };
    // First, see if anything has changed.
    UpdateCmd();
    // Cache flags and draw background.
    const bool bGrayed = CmdFlag (SCmd::cf_Grayed);
    const ptextcols_t cols = color[HasFocus()][bGrayed];
    
    gc.Color (cols[0], cols[1]);
    CWindow::OnDraw (gc);
    if (IsSeparator()) {
	gc.HLine (0, 0, WindowRect().Width());
	return;
    }
    coord_t x = 0;
    // Checkmark for toggle items
    static const wchar_t c_Bullet[2] = { ' ', acsv_Bullet };
    if (CmdFlag(SCmd::cf_Checkable) & !IsInMenuBar())
	gc.Char (x++, 0, c_Bullet[CmdFlag(SCmd::cf_Checked)]);
    gc.Char (x++, 0, ' ');
    // Draw the name.
    x = DrawTextWithAccel (gc, x, 0, Text(), cols);
    gc.Char (x++, 0, ' ');
    // And a submenu indicator
    if (CmdFlag (SCmd::cf_Submenu) & !IsInMenuBar())
	gc.Char (WindowRect().Width() - 1, 0, acsv_RightArrow);
}

/// Responds to keys focused at this item and executes if told.
void CMenuItem::OnKey (wchar_t key)
{
    if ((key == kv_Right) & !CmdFlag (SCmd::cf_Submenu))
	return (Close (cmd_NextMenu));	// Close instead of SetStatus because this is the easest way to special case next menu propagation.
    CButton::OnKey (key);
}

/// Returns the size of the menu item when in \p wr.
void CMenuItem::SizeHints (rrect_t wr) const
{
    wr[1][0] = wr[0][0] + 2 + TextWithAccelSize(Text()) +
		(CmdFlag (SCmd::cf_Checkable) & !IsInMenuBar()) +
		(CmdFlag (SCmd::cf_Submenu) & !IsInMenuBar());
    wr[1][1] = wr[0][1] + 1;
}

/// Updates command flags.
void CMenuItem::UpdateCmd (void)
{
    CButton::UpdateCmd();
    SetFlag (f_NoTabOrder, Flag (f_NoTabOrder) | IsSeparator());
}

