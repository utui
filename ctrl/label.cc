// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// label.cc
//

#include "label.h"

/// Default constructor.
CLabel::CLabel (rctext_t s)
: CWindow (),
  m_Text (s)
{
    SetFlag (f_NoTabOrder);
}

/// Autogen factory for dialog descriptions.
/*static*/ pwidget_t CLabel::Factory (widival_t v)
{
    return (new CLabel (v.text));
}

/// Draws the label in the window.
void CLabel::OnDraw (CGC& gc)
{
    static const EColor colors[3] = { black, cyan, red };
    gc.Color (colors[0], colors[1]);
    CWindow::OnDraw (gc);
    string line;
    coord_t y = 0;
    for (uoff_t i = 0; i < Text().size(); i += line.size() + 1) {
	line.link (Text().iat(i), Text().iat (Text().find ('\n', i)));
	DrawTextWithAccel (gc, 0, y++, line, colors);
    }
}

/// Returns the recommended size for the window.
void CLabel::SizeHints (rrect_t lr) const
{
    Point2d lrbr (lr[1]);
    string line;
    lr[1] = lr[0];
    for (uoff_t i = 0; i < Text().size(); i += line.size() + 1) {
	line.link (Text().iat(i), Text().iat (Text().find ('\n', i)));
	++ lr[1][1];
	lr[1][0] = max (lr[1][0], lr[0][0] + coord_t(TextWithAccelSize(line)));
    }
    simd::pmin (lrbr, lr[1]);
}

/// Draws text \p s with & accelerators in different color.
coord_t CLabel::DrawTextWithAccel (CGC& gc, coord_t x, coord_t y, rctext_t s, ptextcols_t col) const
{
    for (string::utf8_iterator ic = s.utf8_begin(); ic < s.utf8_end(); ++ic) {
	gc.Char (x, y, *ic);
	const bool bAccel (*ic == '&');
	x += !bAccel;
	gc.FgColor (col[bAccel * 2]);
    }
    return (x);
}

/// Returns the size of the label text after contracting & accelerators.
size_t CLabel::TextWithAccelSize (rctext_t s) const
{
    return (s.length() - count(s, '&'));
}

/// \brief Returns the accelerator key for this item.
/// Searches for an & followed by a character, and decode it as utf8.
wchar_t CLabel::FocusAccelKey (void) const
{
    uoff_t ia = Text().find ('&');
    return (ia < Text().size() ? tolower(*utf8in(Text().iat(ia+1))) : 0);
}

