// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// listbox.h
//

#include "listbox.h"

/// Default constructor.
CListbox::CListbox (void)
: m_Selection (0),
  m_TopVisible (0),
  m_ListSize (0)
{
}

/// Draws all the visible list items to \p gc.
void CListbox::OnDraw (CGC& gc)
{
    gc.Color (green, black);
    CWindow::OnDraw (gc);
    Rect wa (WindowArea());
    ++ wa[0][0];
    for (uint32_t i = TopVisible(); i < m_ListSize && wa.Height(); ++i, ++wa[0][1]) {
	if (i == Selection()) {
	    gc.Color (yellow, blue);
	    gc.Bar (0, wa[0][1], gc.Width(), 1);
	}
	OnDrawItem (gc, wa[0], i);
	gc.Color (green, black);
    }
}

/// Processes selector movement keystrokes.
void CListbox::OnKey (wchar_t key)
{
    const uint32_t pageSize (WindowRect().Height());
    CWindow::OnKey (key);
    if (key == kv_Down || key == 'j')
	SetSelection (m_Selection + 1);
    else if (key == kv_Up || key == 'k')
	SetSelection (m_Selection - !!m_Selection);
    else if (key == kv_Home)
	SetSelection (0);
    else if (key == kv_End)
	SetSelection (m_ListSize - 1);
    else if (key == kv_PageUp)
	SetSelection (min (0U, m_Selection - pageSize));
    else if (key == kv_PageDown)
	SetSelection (m_Selection + pageSize);
}

/// Moves the selection to \p i.
void CListbox::SetSelection (uint32_t i)
{
    const uint32_t pageSize (WindowRect().Height());
    i = min (i, m_ListSize - 1);
    m_Selection = i;
    if (m_Selection < m_TopVisible || m_Selection >= m_TopVisible + pageSize)
	m_TopVisible = max (int32_t(m_Selection - pageSize / 2), 0);
    m_TopVisible = min (m_TopVisible, (uint32_t) max (0, int32_t(m_ListSize - pageSize)));
}

