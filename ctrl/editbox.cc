// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// editbox.cc
//

#include "editbox.h"

/// Default constructor.
CEditBox::CEditBox (void)
: CWindow (),
  m_Text (),
  m_Original (),
  m_Pos (0)
{
}

/// Sets the text being edited to \p s.
void CEditBox::SetText (const string& s)
{
    m_Text = m_Original = s;
    m_Pos = 0;
}

/// Draws the currently edited string.
void CEditBox::OnDraw (CGC& gc)
{
    gc.Color (lightgray, blue);
    CWindow::OnDraw (gc);
    string visible;
    if (m_Text.size() <= gc.Width())
	visible.link (m_Text);
    else
	visible.assign (m_Text, 0, gc.Width());
    gc.Text (0, 0, visible);
    CaretTo (m_Pos, 0);
}

/// Handles edit key events.
void CEditBox::OnKey (wchar_t key)
{
    CWindow::OnKey (key);
    switch (key) {
	case kv_Esc:		m_Text = m_Original;
	case kv_Enter:		SetFlag (f_OffersFocus);		break;
	case kv_Right:		m_Pos += (m_Pos < m_Text.size());	break;
	case kv_Left:		m_Pos -= (m_Pos > 0);			break;
	case kv_Backspace:	if (m_Pos) m_Text.erase (--m_Pos);	break;
	case kv_Delete:		if (m_Pos < m_Text.size())
				    m_Text.erase (m_Pos);		break;
	case kv_Home:		m_Pos = 0;				break;
	case kv_End:		m_Pos = m_Text.size();			break;
	default:		if (isprint (key &= kvm_KeyMask))
				    m_Text.insert (m_Pos++, key);	break;
    }
    SetFlag (f_Changed, m_Text != m_Original);
}

