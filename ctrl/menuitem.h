// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// menuitem.h
//

#ifndef MENUITEM_H_1E836CD15086B6DD2ADB329F28B17C15
#define MENUITEM_H_1E836CD15086B6DD2ADB329F28B17C15

#include "button.h"

/// \class CMenuItem ctrl/menuitem.h utui.h
///
/// This is an item in a menu container, capable of drawing the command
/// name, selecting its assigned command, and opening a submenu.
///
class DLL_LOCAL CMenuItem : public CButton {
public:
    inline		CMenuItem (rccmd_t cmd)		: CButton (cmd) { }
    virtual void	UpdateCmd (void);
protected:
    virtual void	OnDraw (CGC& gc);
    virtual void	OnKey (wchar_t key);
    virtual void	SizeHints (rrect_t wr) const;
private:
    inline bool		IsInMenuBar (void) const	{ return (Cmd().parent == cmd_Root); }
    inline bool		IsSeparator (void) const	{ return (Cmd().cmd == cmd_Separator); }
};

#endif

