// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// msgbox.cc
//

#include "msgbox.h"
#include "rootwin.h"

//----------------------------------------------------------------------

enum {
    cmd_MBBase = 0x450A,
    cmd_MsgBoxFirst,
    cmd_Ok = cmd_MsgBoxFirst,
    cmd_Cancel,
    cmd_Yes,
    cmd_No,
    cmd_Abort,
    cmd_Retry,
    cmd_Ignore,
    cmd_MsgBoxLast,
    c_nMsgBoxCmds = cmd_MsgBoxLast - cmd_MsgBoxFirst
};

static const CMessageBox::SCmd s_MBCmds [c_nMsgBoxCmds] = {
    { cmd_Ok,		cmd_MBBase, 0,	"&Ok" },
    { cmd_Cancel,	cmd_MBBase, 0,	"&Cancel" },
    { cmd_Yes,		cmd_MBBase, 0,	"&Yes" },
    { cmd_No,		cmd_MBBase, 0,	"&No" },
    { cmd_Abort,	cmd_MBBase, 0,	"&Abort" },
    { cmd_Retry,	cmd_MBBase, 0,	"&Retry" },
    { cmd_Ignore,	cmd_MBBase, 0,	"&Ignore" }
};

//----------------------------------------------------------------------

#define BEGIN_STD_MESSAGE_BOX(name, nButtons)			\
BEGIN_DIALOG_DESC (name)					\
    LAYOUT_BORDER(1)						\
	LAYOUT_TILE (2, ld_Vertical, 1, 0, 0)			\
	    LAYOUT_ALIGN (1, ld_Horizontal, la_Left, 1, 1)	\
		LABEL ("")					\
	    LAYOUT_ALIGN (1, ld_Horizontal, la_Center, 1, 1)	\
		LAYOUT_TILE (nButtons, ld_Horizontal, 1, 0, 0)

#define MB_BUTTON(name)	BUTTON(s_MBCmds [cmd_##name - cmd_MsgBoxFirst])

BEGIN_STD_MESSAGE_BOX (c_OkDlgDesc, 1)
    MB_BUTTON (Ok)
END_DIALOG_DESC

BEGIN_STD_MESSAGE_BOX (c_OkCancelDlgDesc, 2)
    MB_BUTTON (Ok)
    MB_BUTTON (Cancel)
END_DIALOG_DESC

BEGIN_STD_MESSAGE_BOX (c_YesNoDlgDesc, 2)
    MB_BUTTON (Yes)
    MB_BUTTON (No)
END_DIALOG_DESC

BEGIN_STD_MESSAGE_BOX (c_YesNoCancelDlgDesc, 3)
    MB_BUTTON (Yes)
    MB_BUTTON (No)
    MB_BUTTON (Cancel)
END_DIALOG_DESC

BEGIN_STD_MESSAGE_BOX (c_AbortRetryIgnoreDlgDesc, 3)
    MB_BUTTON (Abort)
    MB_BUTTON (Retry)
    MB_BUTTON (Ignore)
END_DIALOG_DESC

#undef MB_BUTTON
#undef BEGIN_STD_MESSAGE_BOX

//----------------------------------------------------------------------

/// Default constructor.
CMessageBox::CMessageBox (const string& s, EMBType t)
: CDialog (),
  m_Text (s),
  m_Type (t)
{
    static const SWidgetDesc* const c_StdDlgs [MB_Last] = {
	c_OkDlgDesc,
	c_OkCancelDlgDesc,
	c_YesNoDlgDesc,
	c_YesNoCancelDlgDesc,
	c_AbortRetryIgnoreDlgDesc
    };
    InitFromDesc (c_StdDlgs[t]);
    TCW<CLabel>(0).SetText (s);
}

void CMessageBox::OnCommand (cmd_t cmd)
{
    uoff_t cmdIdx = cmd - cmd_MsgBoxFirst;
    if (cmdIdx >= c_nMsgBoxCmds)
	return (CDialog::OnCommand (cmd));
    static const uint8_t c_rvs [c_nMsgBoxCmds] = {
	mbrv_Ok, mbrv_Cancel, mbrv_Yes, mbrv_No, mbrv_Abort, mbrv_Retry, mbrv_Ignore
    };
    Close (c_rvs [cmdIdx]);
}

/// Responds to \p key.
void CMessageBox::OnKey (wchar_t key)
{
    // Enter closes with current button, or the default action (first button)
    switch (key) {
	case kv_Right:	SetFocus (GetNextFocus());	break;
	case kv_Left:	SetFocus (GetNextFocus(-1));	break;
	case kv_Esc:	Close (mbrv_Cancel);		break;
	default:	CDialog::OnKey (key);		break;
    }
}

//----------------------------------------------------------------------

int MessageBox (const string& s, EMBType t)
{
    return (CRootWindow::Instance().RunModal (new CMessageBox (s, t)));
}

//----------------------------------------------------------------------

