// This file is part of the utui library, a terminal UI framework.
//
// Copyright (C) 2006 by Mike Sharov <msharov@users.sourceforge.net>
// This file is free software, distributed under the MIT License.
//
// app.h
//

#ifndef APP_H_226555EB37A021D97E5DB2EB1D8A12CD
#define APP_H_226555EB37A021D97E5DB2EB1D8A12CD

/// \class CApplication app.h utui.h
///
/// This is the main application class base. To create a utui application,
/// derive a singleton class from CApplication and use the WinMain macro
/// to instantiate it.
///
class CApplication {
public:
    typedef int		argc_t;
    typedef const char* const* argv_t;
public:
    inline virtual     ~CApplication (void)		{ }
    int			Run (argc_t argc, argv_t argv);
protected:
			CApplication (void);
    virtual void	OnCreate (argc_t argc, argv_t argv);
    virtual void	OnDestroy (void);
};

//----------------------------------------------------------------------

extern "C" void InstallCleanupHandlers (void);

//----------------------------------------------------------------------

template <typename T>
inline int TWinMain (int argc, const char* const* argv)
{
    InstallCleanupHandlers();
    return (T::Instance().Run (argc, argv));
}

#define WinMain(AppClass)			\
int main (int argc, const char* const* argv)	\
{						\
    return (TWinMain<AppClass> (argc, argv));	\
}

//----------------------------------------------------------------------

#endif

